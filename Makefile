LIBS =
LIBS_PKGCONFIG = rtmidi liblo libuv
SRCS = main.cc \
  App.hpp \
  Midi.hpp \
  MidiTranslator.hpp \
  SooperLooper.hpp \
  ArturiaColors.hpp
BINDIR = .

all: $(SRCS)
	g++ $(SRCS) $(LIBS) -o $(BINDIR)/main `pkg-config --cflags --libs --libs $(LIBS_PKGCONFIG)` -std=gnu++2a

clean:
	rm -f $(BINDIR)/main
