#include <iostream>
#include <stdexcept>
#include <unistd.h> // for usleep
#include <uv.h> // timer for long press
#include <rtmidi/RtMidi.h>

#include "App.hpp"
#include "ArturiaColors.hpp"
#include "MidiTranslator.hpp"
#include "SooperLooper.hpp"

using std::cin;
using std::cout;
using std::endl;

// Global
MidiMessage msg;
RtMidiIn *midiin = 0;
RtMidiOut *midiout = 0;
SooperLooper sl;
ArturiaColors *arturia = 0;
std::vector<unsigned char> message;
bool debug = true;
// Long press
bool is_pad_on = false;
uv_loop_t *loop;
uv_timer_t timer_req;


int usage() {
  cout << "Usage: ./main PORT" << endl;
  cout << "Example ./main 1" << endl;
  return 1;
}


void cleanup() {
  delete midiin;
  delete midiout;
}

void longPressCallback(uv_timer_t* handle) {
  cout << "Checking for long press" << endl;
  if (is_pad_on) {
    cout << "Long press" << endl;
  } else {
    cout << "ERROR: This should not happen" << endl;
  }
}


void printMidiMsg(vector<unsigned char> *message, double deltatime) {
  unsigned int nBytes = message->size();
  for (unsigned int i=0; i<nBytes; i++) {
    std::cout << "Byte " << i << " = " << (int)message->at(i) << ", ";
  }
  if (nBytes > 0) {
    std::cout << "stamp = " << deltatime << std::endl;
  }
};

void handleMidi(double deltatime, std::vector<unsigned char> *message,
  void *userData) {
  if (debug) {
    printMidiMsg(message, deltatime);
  }
  // Translate message
  if (message->size() == 3) {
    MidiTranslator a;
    msg = a.Translate(message->at(0), message->at(1), message->at(2));
    bool redraw_state = false;
    cout << "MSG" << endl;
    switch (msg.type) {
      case NOTE_ON:
        if (msg.note >= 36 && msg.note <= 43) {
          // PAD on
          sl.PadPressed(msg.note-35);
          arturia->PadOn(msg.note-35); // for long press
          redraw_state = true;
        } else {
          //cout << "note on: " << msg.note << endl;
        }
        break;
      case NOTE_OFF:
        redraw_state = true;
        if (msg.note >= 36 && msg.note <= 43) {
          // PAD off
          arturia->PadOff(msg.note-35); // for long press
        }
        //cout << "note off" << endl;
        break;
      case PEDAL_ON_OFF:
        cout << "Pedal on/off" << endl;
        if (msg.data == 127) {
          sl.PedalOn();
        }
        break;
      case AFTERTOUCH:
      case PITCH_WHEEL:
        // TODO
        break;
      default:
        printMidiMsg(message, deltatime);
    } // end switch
    if (redraw_state) {
      SLState state = sl.GetState();
      arturia->RenderState(state);
    }
  } // end if message->size() == 3
}

void printPorts() {
  unsigned int nPorts = midiin->getPortCount();
  std::cout << "\nThere are " << nPorts << " MIDI input sources available.\n";
  std::string portName;
  for (unsigned int i=0; i<nPorts; i++) {
    try {
      portName = midiin->getPortName(i);
    } catch (RtMidiError &error) {
      error.printMessage();
      cleanup();
    }
    std::cout << "  Input Port #" << i+1 << ": " << portName << '\n';
  }

  // Output
  nPorts = midiout->getPortCount();
  if ( nPorts == 0 ) {
    std::cout << "No ports available!\n";
    cleanup();
  }
  std::cout << "\nThere are " << nPorts << " MIDI output ports available.\n";
  for ( unsigned int i=0; i<nPorts; i++ ) {
    try {
      portName = midiout->getPortName(i);
    }
    catch (RtMidiError &error) {
      error.printMessage();
      cleanup();
    }
    std::cout << "  Output Port #" << i+1 << ": " << portName << '\n';
  }
  std::cout << '\n';
  return;
}


int main(int argc, char **argv) {
  //App* app = new App();


  // OLD
  // Input
  try {
    midiin = new RtMidiIn();
  } catch (RtMidiError &error) {
    error.printMessage();
    exit(EXIT_FAILURE);
  }

  // Output
  try {
    midiout = new RtMidiOut();
  } catch (RtMidiError &error) {
    error.printMessage();
    exit(EXIT_FAILURE);
  }

  if (argc == 1) {
    printPorts();
    return usage();
  } else if (argc != 2) {
    return usage();
  }

  arturia = new ArturiaColors(midiout);
  int port = atoi(argv[1]) - 1;

  // Receive
  midiin->openPort(port);
  midiin->setCallback(&handleMidi);
  midiin->ignoreTypes(false, false, false);
  // Wait for init to complete
  while (!sl.init_complete) {
    usleep(10*1000);
  }
  cout << "Init complete" << endl;

  SLState state = sl.GetState();
  arturia->RenderState(state);

  // Event loop
  cout << "Init event loop" << endl;
  loop = uv_default_loop();
  uv_timer_init(loop, &timer_req);
  // Init timers for Arturia
  arturia->InitTimers(loop);
  while (true) {
    uv_run(loop, UV_RUN_DEFAULT);
  }
  uv_loop_close(loop);

  cleanup();
  return 0;
}
