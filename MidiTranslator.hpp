#include <iostream>

using std::cout;
using std::endl;

enum MIDI_EVENT_TYPE {
  NOTE_OFF,
  NOTE_ON,
  AFTERTOUCH,
  PEDAL_ON_OFF,
  PITCH_WHEEL,
};

struct MidiMessage {
  MIDI_EVENT_TYPE type;
  int channel;
  int note;
  int data;
};

class MidiTranslator {
  public:
  MidiTranslator() {}

  MidiMessage Translate(int byte0, int byte1, int byte2) {
    MidiMessage msg;
    int type_base;
    // Message type
    if (byte0 >= 128 && byte0 <= 143) {
      msg.type = NOTE_OFF;
      type_base = 128;
    } else if (byte0 >= 144 && byte0 <= 159) {
      msg.type = NOTE_ON;
      type_base = 144;
    } else if (byte0 >= 160 && byte0 <= 175) {
      msg.type = AFTERTOUCH;
      type_base = 160;
    } else if (byte0 >= 176 && byte0 <= 191) {
      msg.type = PEDAL_ON_OFF;
      type_base = 176;
    } else if (byte0 == 224) {

    }
    // bug: final else missing
    msg.channel = byte0 - type_base + 1;
    msg.note = byte1;
    msg.data = byte2;
    return msg;
  }
}; // class MidiTranslator
