#include <iostream>
#include <map>
#include <string>
#include <uv.h> // for timers

#include <rtmidi/RtMidi.h>

#include "SooperLooper.hpp"

using std::cout;
using std::endl;
using std::map;
using std::string;
using std::vector;

map<string, unsigned char> colors = {
  { "none", 0x00 },
  { "red", 0x01 },
  { "green", 0x04 },
  { "yellow", 0x05 },
  { "blue", 0x10 },
  { "pink", 0x11 },
  { "turquoise", 0x14 },
  { "white", 0x7F },
};

// pad: flashing: on/off, state: on/off, color: RGB/none
// sustain: state: on/off
struct ButtonState {
  bool is_pressed;
  bool is_flashing;
  unsigned char color;
  uv_timer_t timer;
};

struct State {
  bool is_pressed_sustain;
  vector<ButtonState> pads;
};


class ArturiaColors {
  struct TimerData {
    int pad_number;
    ArturiaColors* instance;
  };
  public:
  ArturiaColors(RtMidiOut* _midi):
    state({
      .is_pressed_sustain = false,
      .pads = {}, // vector of pads: is_pressed,is_flashing=false, color=off
    })
  {
    midi = _midi;
    midi->openPort(1); // TODO: Fix

    // Populate pads status
    for (int i = 0; i < pad_count; i++) {
      state.pads.push_back(ButtonState {
        false, // is_pressed
        false, // is_flashing
        0x00, // color
        (uv_timer_t *) malloc(sizeof(uv_timer_t)) // timer
      });
    }
  }




  void InitTimers(uv_loop_t* _loop) {
    loop = _loop;
    cout << "Init timers" << endl;
    // TODO: How to put state.pads[i].timer into a var?
    for (int i = 0; i < pad_count; i++) {
      // Each timer has a data field with the index of the timer
      uv_timer_init(loop, &state.pads[i].timer);
      uv_handle_t* handle = (uv_handle_t*) &state.pads[i].timer;
      uv_handle_set_data(handle, new TimerData { i, this });
    }
    cout << "Timers done" << endl;
  };


  void PadOff(int pad) {
    uv_timer_stop(&state.pads[pad].timer);
    uv_handle_t* handle = (uv_handle_t*) &state.pads[pad].timer;
    if (uv_is_active(handle) && !uv_is_closing(handle)) {
      uv_close(handle, NULL);
    }
    state.pads[pad].is_pressed = false;
  };


  void PadOn(int pad) {
    state.pads[pad].is_pressed = true;
    uv_timer_init(loop, &state.pads[pad].timer);
    uv_timer_start(&state.pads[pad].timer, _PadTimerCallback, 1000, 0);
  };


  void RenderState(SLState state) {
    std::vector<unsigned char> msg;
    for (int i = 0; i < state.loops.size(); i++) {
      if (i == state.selected_loop) {
        // Highlight current loop
        msg = ColorMessage(i, colors["blue"]);
      } else {
        // No color
        msg = ColorMessage(i, colors["none"]);
      }
      midi->sendMessage(&msg);
    }
    //for (auto loop : state.loops) {}
  };

  std::vector<unsigned char> ColorMessage(int _button, int color) {
    // color:
    // 0x00 - black (off)
    // 0x01 - red
    // 0x04 - green
    // 0x05 - yellow
    // 0x10 - blue
    // 0x11 - magenta
    // 0x14 - cyan
    // 0x7F - white
    std::vector<unsigned char> message;
    int button = 0x70 + _button;
    message.clear();
    message.push_back(0xF0);
    message.push_back(0x00);
    message.push_back(0x20);
    message.push_back(0x6B);
    message.push_back(0x7F);
    message.push_back(0x42);
    message.push_back(0x02);
    message.push_back(0x00);
    message.push_back(0x10);
    message.push_back(button); // button
    message.push_back(color); // color
    message.push_back(0xF7);
    return message;
  }

  protected:
  static void _PadTimerCallback(uv_timer_t* handle) {
    TimerData *td = (TimerData*) handle->data;
    ArturiaColors *ac = (ArturiaColors*) td->instance;
    ac->PadTimerCallback(td->pad_number);
  };

  void PadTimerCallback(int pad_number) {
    if (state.pads[pad_number].is_pressed) {
      cout << "LONG PRESS FOR PAD: " << pad_number << endl;
    }
  };

  private:
    RtMidiOut* midi;
    State state;
    int pad_count = 8;
    uv_loop_t* loop;
};
