#ifndef SOOPERLOOPER_HPP
#define SOOPERLOOPER_HPP

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <unistd.h> // for usleep
#include <vector>

#include <lo/lo.h>
#include <lo/lo_cpp.h>

using std::cin;
using std::cout;
using std::endl;
using std::stringstream;
using std::string;
using std::vector;


enum LoopState {
  EMPTY,
  RECORDING,
  RECORDED,
};

struct Loop {
  LoopState state;
};

struct SLState {
  int          selected_loop;
  vector<Loop> loops;
};


class SooperLooper {
  public:
  bool init_complete = false;

  SooperLooper():
    osc_listener_port(9000),
    osc_listener(osc_listener_port),
    osc_client("localhost", "9951"),
    state({
      .selected_loop = 0,
      .loops = {
        { .state = EMPTY },
        { .state = EMPTY },
        { .state = EMPTY },
        { .state = EMPTY },
        { .state = EMPTY },
      },
    })
  {
    cout << "Init SL" << endl;
    if (!osc_listener.is_valid()) {
      std::cout << "Could not start SooperLooper server." << std::endl;
      throw std::runtime_error("Could not start return server");
    }
    std::cout << "Listening on: " << osc_listener.url() << endl;

    // Listener methods
    osc_listener.add_method("/ping_init", "ssi", SooperLooper::_handle_ping_init, this);
    osc_listener.start();

    // Init loops
    osc_client.send("/ping", "ss", "localhost:9000", "/ping_init");
  }


  SLState GetState() {
    return state;
  };


  void Init(int count_loops) {
    // Delete loops, create new loops, set sync etc
    cout << "Initialise new session" << endl;
    for (int i = 0; i < count_loops; i++) {
      // Delete all loops
      osc_client.send("/loop_del", "i", -1);
      usleep(10*10000); // TODO: Why do we need to wait?
    }
    for (int i = 0; i < 5; i++) {
      // Create 5 loops
      osc_client.send("/loop_add", "if", 1, 47.55f);
    }
    // Quantize and sync all loops
    usleep(10*10000); // TODO: Why do we need to wait?
    osc_client.send("/sl/-1/set", "sf", "sync", (float) 1);
    osc_client.send("/sl/-1/set", "sf", "quantize", (float) 3);
  };


  void PadPressed(int pad) {
    SelectLoop(pad-1);
  }


  void PedalOn() {
    // Record on selected loop, or, if already recorded, Overdub
    LoopState loop_state = state.loops[state.selected_loop].state;
    if (loop_state == EMPTY) {
      cout << "Pressed record on empty loop" << endl;
      SetLoopState(state.selected_loop, RECORDING);
      Record();
    } else if (loop_state == RECORDING) {
      cout << "Pressed record on recording loop, so finish recording" << endl;
      Record();
      SetLoopState(state.selected_loop, RECORDED);
    } else {
      cout << "Pressed record on recorded loop, so Overdub" << endl;
      Overdub();
    }
  };


  void Record() {
    if (osc_client.send("/sl/-3/hit", "s", "record") == -1) {
      cout << "ERROR" << endl;
    }
  }


  void Overdub() {
    if (osc_client.send("/sl/-3/hit", "s", "overdub") == -1) {
      cout << "ERROR" << endl;
    }
  }


  void SelectLoop(int loop) {
    if (osc_client.send("/set", "sf", "selected_loop_num", (float)loop) == -1) {
      cout << "ERROR" << endl;
    }
    state.selected_loop = loop;
  }

  void SetLoopState(int loop, LoopState _state) {
    state.loops[loop].state = _state;
  }


  /* PROTECTED */
  protected:
  static int _handle_ping_init(const char *path, const char *types, lo_arg **argv,
    int argc, void *data, void *user_data)
  {
    SooperLooper * lc = static_cast<SooperLooper*> (user_data);
    return lc->handle_ping_init(path, types, argv, argc, data);
  }

  int handle_ping_init(const char *path, const char *types, lo_arg **argv,
    int argc, void *data)
  {
    int count_loops = argv[2]->i;
    cout << "Loops found: " << count_loops << endl;
    //  << &argv[1]->s << " "
    //  << argv[2]->i << endl;
    string init_new_session = "";
    if (count_loops != 0) {
      while (init_new_session != "y" && init_new_session != "n") {
        cout << "Initialise new session? (y/n): ";
        getline(cin, init_new_session);
      }
      if (init_new_session == "y") {
        Init(count_loops);
      }
    }
    SelectLoop(0);
    init_complete = true;
    return 0;
  }


  /* PRIVATE */
  private:
  int osc_listener_port;
  lo::ServerThread osc_listener;
  lo::Address osc_client;
  SLState state;

}; // end class SooperLooper

#endif
