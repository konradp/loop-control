# loop-control
https://konradp.gitlab.io/blog/post/com-loop-control/

# MIDI messages
byte | data
--- | ---
0 | event + channel
1 | note
2 | velocity

## Examples
event | data (byte0/byte1/byte2)
--- | ---
ch1, C3 off    | `128/48/00`
ch1, C3 on     | `144/48/53`
ch2, C3 off    | `129/48/53`
ch2, C3 on     | `145/48/53`
ch1, C-1 on    | `144/0/53`
ch1, C-1 off   | `128/0/0`
ch1, C0 on     | `144/12/53`
ch1, C0 off    | `128/12/0`
ch1, C1 on     | `144/24/53`
ch1, C1 off    | `128/24/0`
ch1, ped on    | `176/64/127`
ch1, ped off   | `176/64/0`
ch10, pad1 on  | `153/36/53`
ch10, pad1 off | `137/36/0`
ch10, pad2 on  | `153/37/53`
ch10, pad3 on  | `153/38/53`
ch10, pad4 on  | `153/39/53`
ch10, pad5 on  | `153/40/53`
ch10, pad6 on  | `153/41/53`
ch10, pad7 on  | `153/42/53`
ch10, pad8 on  | `153/43/53`


## Events: Byte 0

- note on: 144+(channel-1)
- note off: 128+(channel-1)
- aftertouch: 160+(channel-1)

Examples:

event | value
--- | ---
ch1,  note on         | 144
ch2,  note on         | 145
ch10, note on (pad1)  | 153
ch10, note off (pad1) | 137
ch16, note on         | 144+(16-1)=159
ch16, note off        | 128+15=143
damper pedal          | 176 (on: byte2=127, off: byte2=0)

So, ranges are, for all 16 channels:
- note off: 128 - 143
- note on: 144 - 159
- aftertouch: 160 - 175
- pedal on/off: 176 - 191
